import requests
import xml.etree.ElementTree as ET
from flask import Flask, escape, request


app = Flask(__name__)

# HTTP GET at oath /
@app.route('/')
def hello():

	#xml file from the city of Austin
	pools_xml = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"

	#root is the top of the xml file, a row
	response = requests.get(pools_xml)
	root = ET.fromstring(response.text)


	if response:
		print('Success!')
	else:
		print('An error has occurred.')

	#string to hold html
	output_string = ""

	#collect expected arguments
	param_weekend = request.args.get('weekend')
	param_pool_type = request.args.get('pool_type')
	param_weekday_closure= request.args.get('weekday_closure')
	#print(weekend, type(weekend))
    
    #add pools to return file if it matches any query	
	for child in root:
		#print(child.find("pool_name").text)
		matches = True
		
		if param_weekend is not None:
			weekend = child.find('weekend')
			if (weekend is not None and weekend.text != param_weekend) or weekend is None:
				#print("weekend", child.find("pool_name").text)
				matches = False
				
		
		if param_pool_type is not None:
			pool_type = child.find('pool_type')
			if (pool_type is not None and pool_type.text != param_pool_type) or pool_type is None:
				#print("type", child.find("pool_name").text)
				matches = False
				
		if param_weekday_closure is not None:
			weekday_closure = child.find('weekday_closure')
			if (weekday_closure is not None and weekday_closure.text != param_weekday_closure) or weekday_closure is None:
				matches = False
				#print("weekday", child.find("pool_name").text)
		
		if param_weekend is None and param_pool_type is None and param_weekday_closure is None:
			matches = False
			#print("All None")
		
		if matches:
			output_string += child.find("pool_name").text + "<br>"
		
		
		
	if output_string == "":
		output_string = "<h1>Welcome to Austin Pool Information Website.</h1>"

	return output_string

# Starts the web server
if __name__ == '__main__':
	app.run()
